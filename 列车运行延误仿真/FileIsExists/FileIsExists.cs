﻿using System;
using System.IO;
using System.Windows.Forms;

namespace FileIsExists
{
    public partial class FileIsExists : Form
    {
        public FileIsExists()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] fileNames = {"background.jpg","Di_Tu.jpg","DrawPicture.exe",
            "train_timetable.mdb","Calculate.dll","System.dll","System.Drawing.dll",
            "System.Windows.Forms.dll","Microsoft.CSharp.dll"};
            string NotFoundList = null;
            foreach (string Name in fileNames)
            {
                string FilePath = Application.StartupPath + "\\" + Name;
                if (File.Exists(FilePath) == false)
                    NotFoundList += (Name + " ");
            }
            if (NotFoundList != null)
                MessageBox.Show(NotFoundList + "文件丢失", "提示", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            Environment.Exit(0);
        }
    }
}
