using System;

namespace Calculate
{
    public static class Delay
    {
        public static int departure_interval = 6;       //发车间隔时间,min
        public static int start_interval_buffer = 1;    //发车间隔缓冲时间,min
        public static int interval_running = 6;         //区间运行时间,min
        public static int interval_running_buffer = 20; //区间运行缓冲时间,s
        public static int station_dwell = 20;           //停站时间,s
        public static int station_dwell_buffer = 10;    //停站缓冲时间,s  

        //离开延误
        public static (int[], int[]) Leave_Delay(int delayTime_min, int delayTime_s)
        {
            int delaytime = delayTime_min * 60 + delayTime_s;
            int i, k;
            int[] leave_delay_time; int[] interval_delay_time;     //离开延迟与运行延迟

            int m = delaytime / 30;
            int n = delaytime % 30;
            if (m == 0)
            {
                i = k = 1;
                leave_delay_time = new int[i];
                interval_delay_time = new int[k];
                if (n < interval_running_buffer)
                {
                    interval_delay_time[0] = n;
                    leave_delay_time[0] = 0;
                }
                else
                {
                    interval_delay_time[0] = interval_running_buffer;
                    leave_delay_time[0] = n - interval_running_buffer;
                }
            }
            else
            {
                i = k = m;
                if (n == 0)
                {
                    interval_delay_time = new int[i];
                    leave_delay_time = new int[k];
                    for (int j = 0; j < m; j++)
                    {
                        interval_delay_time[j] = interval_running_buffer;
                        leave_delay_time[j] = station_dwell_buffer;
                    }
                }
                else
                {
                    if (n < interval_running_buffer)
                    {
                        i++;
                        interval_delay_time = new int[i];
                        leave_delay_time = new int[k];
                        for (int j = 0; j < k; j++)
                        {
                            interval_delay_time[j] = interval_running_buffer;
                            leave_delay_time[j] = station_dwell_buffer;
                        }
                        interval_delay_time[i - 1] = n;
                    }
                    else
                    {
                        i = k = m + 1;
                        interval_delay_time = new int[i];
                        leave_delay_time = new int[k];
                        for (int j = 0; j < m; j++)
                        {
                            interval_delay_time[j] = interval_running_buffer;
                            leave_delay_time[j] = station_dwell_buffer;
                        }
                        interval_delay_time[m] = interval_running_buffer;
                        leave_delay_time[m] = n - interval_running_buffer;
                    }
                }
            }

            return (interval_delay_time, leave_delay_time);
        }

        //到达延误
        public static (int[], int[]) Arrival_Delay(int delayTime_min, int delayTime_s)
        {
            int delaytime = delayTime_min * 60 + delayTime_s;
            int i, k;
            int[] arrival_delay_time; int[] interval_delay_time;     //到达延迟与运行延迟
            
            int m = delaytime / 30;
            int n = delaytime % 30;
            if (m == 0)
            {
                i = k = 1;
                arrival_delay_time = new int[i];
                interval_delay_time = new int[k];
                if (n < station_dwell_buffer)
                {
                    arrival_delay_time[0] = n;
                    interval_delay_time[0] = 0;
                }
                else
                {
                    arrival_delay_time[0] = station_dwell_buffer;
                    interval_delay_time[0] = n - station_dwell_buffer;
                }
            }
            else
            {
                i = k = m;
                if (n == 0)
                {
                    arrival_delay_time = new int[i];
                    interval_delay_time = new int[k];
                    for (int j = 0; j < m; j++)
                    {
                        arrival_delay_time[j] = station_dwell_buffer;
                        interval_delay_time[j] = interval_running_buffer;
                    }
                }
                else
                {
                    if (n < station_dwell_buffer)
                    {
                        i++;
                        arrival_delay_time = new int[i];
                        interval_delay_time = new int[k];
                        for (int j = 0; j < i - 1; j++)
                        {
                            arrival_delay_time[j] = station_dwell_buffer;
                            interval_delay_time[j] = interval_running_buffer;
                        }
                        arrival_delay_time[i - 1] = n;
                    }
                    else
                    {
                        i = k = m + 1;
                        arrival_delay_time = new int[i];
                        interval_delay_time = new int[k];
                        for (int j = 0; j < m; j++)
                        {
                            arrival_delay_time[j] = station_dwell_buffer;
                            interval_delay_time[j] = interval_running_buffer;
                        }
                        arrival_delay_time[m] = station_dwell_buffer;
                        interval_delay_time[m] = n - station_dwell_buffer;
                    }
                }
            }
            return (arrival_delay_time, interval_delay_time);
        }
    }
}