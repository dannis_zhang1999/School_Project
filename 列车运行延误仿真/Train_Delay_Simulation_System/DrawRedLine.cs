﻿using Calculate;
using System;
using System.Drawing;
using System.Windows.Forms;
using static TDSS.DataStore;
using static TDSS.STATIC;

namespace TDSS
{
    class DrawRedLine
    {
        internal static string[] Station = new string[]
        {
            "杨高中路", "世纪大道", "商城路", "小南门", "陆家浜", "马当路", "打浦桥", "嘉善路",
            "肇家浜", "徐家汇", "宜山路", "桂林路", "漕河泾开发区", "合川路", "星中路", "七宝",
            "中春路", "九亭", "泗泾", "佘山", "洞泾", "松江大学城", "松江新城", "松江体育中心",
            "醉白池", "松江南站"
        };
        internal static int[] stations = {38, 58, 70, 90, 103, 113, 125, 136, 147, 160, 174, 188, 205,
                                         217, 233, 250, 262, 283, 324, 352, 369, 393, 415, 431, 448, 463};

        internal static (Point[], Point, Point, int, int) draw(string type, int min, int s, string station, string dir, int trainnumber)
        {
            Point[] result;
            Point point1 = new Point();
            Point point2 = new Point();
            int usefulLength;
            int consumed_delaytime;
            int delayhour = new_turn(min, s).Item1;
            int delaymin = new_turn(min, s).Item2;
            int delays = new_turn(min, s).Item3;
            string delay_time = turn(delayhour, delaymin, delays);
            bool DO;

            int n = -1;//延误车站编号
            for (int i = 0; i < 26; i++)
            {
                if (Equals(station, Station[i]))
                {
                    n = i;
                    break;
                }
            }
            if (n == -1)
                throw new Exception("未选择车站");

            if (Equals(type, "离开延误"))
            {
                #region
                int[] interval_delaytime = new int[Delay.Leave_Delay(min, s).Item1.Length];
                Array.Copy(Delay.Leave_Delay(min, s).Item1, interval_delaytime, interval_delaytime.Length);
                int[] leave_delaytime = new int[Delay.Leave_Delay(min, s).Item2.Length];
                Array.Copy(Delay.Leave_Delay(min, s).Item2, leave_delaytime, leave_delaytime.Length);

                int[] vs = new int[interval_delaytime.Length + leave_delaytime.Length];
                add(ref vs, interval_delaytime, leave_delaytime);               
                #endregion

                if (Equals(dir, "往松江南站方向"))//往松江南站方向
                {
                    if (trainnumber > 175)
                        throw new OverflowException("列车编号超出允许范围");

                    usefulLength = MIN(vs.Length, 49 - 2 * n);
                    result = new Point[usefulLength + 1];

                    string the_first;
                    if (n > 0 && n < 25)
                    {
                        the_first = Time_table.Tables[0].Rows[trainnumber - 1][2 * n + 2].ToString();
                        point2.X = result[0].X = Abscissa(Time_Add(the_first, delay_time));
                        point2.Y = result[0].Y = stations[n];
                        point1.X = Abscissa(the_first);
                        point1.Y = stations[n];
                        assignment(Time_Add(the_first, delay_time), delay_time);
                        DO = true;
                    }
                    else if (n < 0 || n > 25)
                        throw new IndexOutOfRangeException("输入数据不合法");
                    else
                    {
                        if (AbnormalMessagePrompt == true)
                            MessageBox.Show("列车已经在终点站，延误设置不成立");
                        DO = false;
                    }

                    if (DO == true)
                    {
                        string the_data;
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            the_data = Time_table.Tables[0].Rows[trainnumber - 1][2 * n + 2 + i].ToString();
                            delay_time = Time_Sub(delay_time, turn(0, 0, vs[i - 1]));
                            result[i].X = Abscissa(Time_Add(the_data, delay_time));
                            result[i].Y = stations[n + (i + 1) / 2];
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            result[i].X = result[0].X;
                            result[i].Y = result[0].Y;
                        }
                    }
                    consumed_delaytime = (result[usefulLength].X - result[1].X) + (point2.X - point1.X);
                }
                else//往杨高中路方向
                {
                    if (trainnumber > 169)
                        throw new OverflowException("列车编号超出允许范围");

                    usefulLength = MIN(vs.Length, 2 * n - 1);
                    result = new Point[usefulLength + 1];

                    string the_first;
                    if (n > 0)
                    {
                        the_first = Time_table.Tables[1].Rows[trainnumber - 1][-2 * n + 52].ToString();
                        point2.X = result[0].X = Abscissa(Time_Add(the_first, delay_time));
                        point2.Y = result[0].Y = stations[n];
                        point1.X = Abscissa(the_first);
                        point1.Y = stations[n];
                        assignment(Time_Add(the_first, delay_time), delay_time);
                        DO = true;
                    }
                    else if (n < 0)
                        throw new IndexOutOfRangeException("输入数据不合法");
                    else
                    {
                        if (AbnormalMessagePrompt == true)
                            MessageBox.Show("列车已经在终点站，延误设置不成立");
                        DO = false;
                    }

                    if (DO == true)
                    {
                        string the_data;
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            the_data = Time_table.Tables[1].Rows[trainnumber - 1][-2 * n + 52 + i].ToString();
                            delay_time = Time_Sub(delay_time, turn(0, 0, vs[i - 1]));
                            result[i].X = Abscissa(Time_Add(the_data, delay_time));
                            result[i].Y = stations[n - (i + 1) / 2];
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            result[i].X = result[0].X;
                            result[i].Y = result[0].Y;
                        }
                    }
                    consumed_delaytime = (result[usefulLength].X - result[1].X) + (point2.X - point1.X);
                }
            }
            else//到达延误
            {
                #region
                int[] arrival_delaytime = new int[Delay.Arrival_Delay(min, s).Item1.Length];
                Array.Copy(Delay.Leave_Delay(min, s).Item1, arrival_delaytime, arrival_delaytime.Length);
                int[] interval_delaytime = new int[Delay.Arrival_Delay(min, s).Item2.Length];
                Array.Copy(Delay.Leave_Delay(min, s).Item2, interval_delaytime, interval_delaytime.Length);

                int[] vs = new int[arrival_delaytime.Length + interval_delaytime.Length];
                add(ref vs, arrival_delaytime, interval_delaytime);
                #endregion

                if (Equals(dir, "往松江南站方向"))//往松江南站方向
                {
                    if (trainnumber > 175)
                        throw new OverflowException("列车编号超出允许范围");

                    usefulLength = MIN(vs.Length, -2 * n + 50);
                    result = new Point[usefulLength + 1];

                    string the_first;
                    if (n < 25 && n > 0)
                    {
                        the_first = Time_table.Tables[0].Rows[trainnumber - 1][2 * n + 1].ToString();
                        point2.X = result[0].X = Abscissa(Time_Add(the_first, delay_time));
                        point2.Y = result[0].Y = stations[n];
                        point1.X = Abscissa(the_first);
                        point1.Y = stations[n];
                        DO = true;
                        actual_departure_time = Time_Add(Time_table.Tables[0].Rows[trainnumber - 1][2 * n + 2].ToString(), turn(delayhour, delaymin, delays));
                    }
                    else if (n < 0 || n > 25)
                        throw new IndexOutOfRangeException("输入数据不合法");
                    
                    else
                    {
                        if (AbnormalMessagePrompt == true)
                            MessageBox.Show("列车已经在终点站，延误设置不成立");
                        DO = false;
                    }

                    if (DO == true)
                    {
                        string the_data;
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            the_data = Time_table.Tables[0].Rows[trainnumber - 1][2 * n + 1 + i].ToString();
                            delay_time = Time_Sub(delay_time, turn(0, 0, vs[i - 1]));
                            result[i].X = Abscissa(Time_Add(the_data, delay_time));
                            result[i].Y = stations[n + i / 2];
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            result[i].X = result[0].X;
                            result[i].Y = result[0].Y;
                        }
                    }
                    consumed_delaytime = (result[usefulLength].X - result[1].X) + (point2.X - point1.X);
                }
                else//往杨高中路方向
                {
                    if (trainnumber > 169)
                        throw new OverflowException("列车编号超出允许范围");

                    usefulLength = MIN(vs.Length, 2 * n);
                    result = new Point[usefulLength + 1];

                    string the_first;
                    if (n > 0)
                    {
                        the_first = Time_table.Tables[1].Rows[trainnumber - 1][-2 * n + 51].ToString();
                        point2.X = result[0].X = Abscissa(Time_Add(the_first, delay_time));
                        point2.Y = result[0].Y = stations[n];
                        point1.X = Abscissa(the_first);
                        point1.Y = stations[n];
                        DO = true;
                        actual_departure_time = Time_Add(Time_table.Tables[1].Rows[trainnumber - 1][-2 * n + 52].ToString(), turn(delayhour, delaymin, delays));
                    }
                    else if (n < 0)
                        throw new IndexOutOfRangeException("输入数据不合法");
                    else
                    {
                        if (AbnormalMessagePrompt == true)
                            MessageBox.Show("已经在终点站，延误设置不成立");
                        DO = false;
                    }

                    if (DO == true)
                    {
                        string the_data;
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            the_data = Time_table.Tables[1].Rows[trainnumber - 1][-2 * n + 51 + i].ToString();
                            delay_time = Time_Sub(delay_time, turn(0, 0, vs[i - 1]));
                            result[i].X = Abscissa(Time_Add(the_data, delay_time));
                            result[i].Y = stations[n - i / 2];
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= usefulLength; i++)
                        {
                            result[i].X = result[0].X;
                            result[i].Y = result[0].Y;
                        }
                    }
                    consumed_delaytime = (result[usefulLength].X - result[1].X) + (point2.X - point1.X);
                }
            }
            int affected_train = consumed_delaytime / (12 * 6);
            return (result, point1, point2, usefulLength, affected_train);
        }

        #region
        //时间数据格式转化方法
        internal static string turn(int h, int min, int s)
        {
            string s1, s2, s3;
            s1 = turn(h);
            s2 = turn(min);
            s3 = turn(s);
            return s1 + ":" + s2 + ":" + s3;
        }
        private static string turn(int n)
        {
            string r;
            if (n < 10)
                r = "0" + n.ToString();
            else
                r = n.ToString();
            return r;
        }

        internal static (int, int, int) new_turn(int min, int s)
        {
            int new_hour, new_min, new_s;
            int sum = min * 60 + s;
            new_hour = sum / 3600;
            new_min = sum % 3600 / 60;
            new_s = sum % 60;
            return (new_hour, new_min, new_s);
        }

        internal static int MIN(int a, int b)
        {
            return Math.Min(a, b);
        }

        internal static void add(ref int[] a, int[] value1, int[] value2)//交错合并数组
        {
            try
            {
                if (a.Length % 2 == 0)
                {
                    for (int i = 0; i < a.Length; i += 2)
                    {
                        a[i] = value1[i / 2];
                        a[i + 1] = value2[i / 2];
                    }
                }
                else
                {
                    for (int i = 0; i < a.Length - 1; i += 2)
                    {
                        a[i] = value1[i / 2];
                        a[i + 1] = value2[i / 2];
                    }
                    a[a.Length - 1] = value1[value1.Length - 1];
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
