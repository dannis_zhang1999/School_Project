﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;
using static TDSS.DataStore;

namespace TDSS
{
    public partial class Import_Schedule : Form
    {
        public Import_Schedule()
        {
            InitializeComponent();
        }

        DataSet ds = new DataSet();
        static int a = 0;
        private void Import_Schedule_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;
          
            try
            {               
                string strPath = Application.StartupPath + "\\train_timetable.mdb";
                string Connect = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strPath;
                string ConStr = string.Format(Connect, Application.StartupPath);
                OleDbConnection oleCon = new OleDbConnection(ConStr);                                   
                OleDbDataAdapter oleDap1 = new OleDbDataAdapter("select * from train_timetable1", oleCon);
                OleDbDataAdapter oleDap2 = new OleDbDataAdapter("select * from train_timetable2", oleCon);
                oleDap1.Fill(ds, "train_timetable1");
                oleDap2.Fill(ds, "train_timetable2");
                Get_data(ds);
                if (a == 0)
                {
                    MessageBox.Show("已连接数据库", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    a++;
                }

                oleDap1.Dispose();
                oleDap2.Dispose();
                oleCon.Close();
                oleCon.Dispose();
            }
            catch (OleDbException ex)
            {
                MessageBox.Show(ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }            
        }

        private void Parameters2_Click(object sender, EventArgs e)
        {
            Setup_Parameters setup_Parameters = new Setup_Parameters();
            this.Hide();
            setup_Parameters.ShowDialog();
        }

        private void Diagram2_Click(object sender, EventArgs e)
        {
            Generate_Diagram generate_Diagram = new Generate_Diagram();
            this.Hide();
            generate_Diagram.ShowDialog();
        }

        private void ImportSchedule_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = ds.Tables[0].DefaultView;
            }
            else if (radioButton2.Checked)
            {
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = ds.Tables[1].DefaultView;
            }
            else
                MessageBox.Show("尚未选择时刻表");
        }
    }
}