﻿//2019-12-29, Created by Dannis Zhang, from SUES

using System;
using System.Windows.Forms;

namespace TDSS
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Start_Interface());
        }
    }
}