using System;
using System.Windows.Forms;
using static TDSS.DataStore;
using static TDSS.DrawRedLine;

namespace TDSS
{
    public partial class Setup_Parameters : Form
    {
        public Setup_Parameters()
        {
            InitializeComponent();
        }

        private bool Data_Save = false;   //判断修改后的数据是否已经保存
        private static string delayTrain,delayTime_min,delayTime_s,delaySite,delayType,direction;

        private void Setup_Parameters_Load(object sender, EventArgs e)
        {            
            this.MaximizeBox = false;

            train.Text = delayTrain;
            min.Text = delayTime_min;
            s.Text = delayTime_s;
            site.SelectedItem = delaySite;
            type.SelectedItem = delayType;
            directioncombox.SelectedItem = direction;
        }

        Generate_Diagram generate_Diagram = new Generate_Diagram();
        Import_Schedule import_Schedule = new Import_Schedule();

        private void Schedule2_Click(object sender, EventArgs e)
        {           
            if (Data_Save == false)
            {
                DialogResult d = MessageBox.Show("数据尚未保存，是否跳转", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                if (d == DialogResult.OK)
                {
                    this.Hide();
                    import_Schedule.ShowDialog();
                }
                else
                    return;
            }
            else
            {
                this.Hide();
                import_Schedule.ShowDialog();
            }
        }
        
        private void SaveData_Click(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                if (control is TextBox || control is ComboBox)
                {
                    if (String.IsNullOrEmpty(control.Text) || control == null)
                    {
                        string data_name;
                        if (Equals(control.Name, "train"))
                        {
                            data_name = "延误车次未设置";
                            MessageBox.Show(data_name, "参数未设置", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (Equals(control.Name, "min"))
                        {
                            data_name = "延误时间（分钟）未设置";
                            MessageBox.Show(data_name, "参数未设置", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (Equals(control.Name, "s"))
                        {
                            data_name = "延误时间（秒）未设置";
                            MessageBox.Show(data_name, "参数未设置", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (Equals(control.Name, "site"))
                        {
                            data_name = "延误站点未设置";
                            MessageBox.Show(data_name, "参数未设置", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (Equals(control.Name, "type"))
                        {
                            data_name = "延误类型未设置";
                            MessageBox.Show(data_name, "参数未设置", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else if (Equals(control.Name, "directioncombox"))
                        {
                            data_name = "运行方向未设置";
                            MessageBox.Show(data_name, "参数未设置", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else
                        { }
                    }
                    else if (control is TextBox && !System.Text.RegularExpressions.Regex.IsMatch(control.Text, @"^[0-9]*$"))
                    {
                        MessageBox.Show("数据不合法", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else if (control is TextBox && int.Parse(control.Text) < 0)
                    {
                        MessageBox.Show("数据不合法", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }

            delayTrain = train.Text.Trim();
            delayTime_min = min.Text.Trim();
            delayTime_s = s.Text.Trim();
            delaySite = site.SelectedItem;
            delayType = type.SelectedItem;
            direction = directioncombox.SelectedItem;

            STATIC.Train = delayTrain;
            int h, m, s_;
            h = new_turn(int.Parse(delayTime_min), int.Parse(delayTime_s)).Item1;
            m = new_turn(int.Parse(delayTime_min), int.Parse(delayTime_s)).Item2;
            s_ = new_turn(int.Parse(delayTime_min), int.Parse(delayTime_s)).Item3;
            STATIC.DELAYTIME = turn(h, m, s_);

            try
            {
                Get_data(delayTrain, delayTime_min, delayTime_s, delaySite.ToString(), delayType.ToString(), direction.ToString());
                MessageBox.Show("数据已保存", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Data_Save = true;                
            }
            catch (NullReferenceException)
            {
                DialogResult d = MessageBox.Show("有参数未设置，是否继续？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (d == DialogResult.OK)
                {
                    this.Hide();
                    generate_Diagram.ShowDialog();
                    Data_Save = false;
                }
                else
                    return;
            }

            this.Hide();
            generate_Diagram.ShowDialog();
        }
    }
}