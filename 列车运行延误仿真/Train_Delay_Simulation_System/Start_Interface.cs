﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace TDSS
{
    public partial class Start_Interface : Form
    {
        public Start_Interface()
        {
            InitializeComponent();
        }

        private void Start_Interface_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;  //禁用窗体最大化按钮

            try
            {
                Process pro = new Process();
                pro.StartInfo.FileName = Application.StartupPath + "\\FileIsExists.exe";
                pro.Start();
                pro.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Environment.Exit(-1);
            }

            string s = "欢迎使用列车运行延误仿真系统";
            Font myFont = new Font("华文行楷", 50);
            SolidBrush myBrush = new SolidBrush(Color.Red);
            Graphics myGraphics = Graphics.FromImage(this.BackgroundImage);
            myGraphics.DrawString(s, myFont, myBrush, 160, 180);
        }

        private void Start_Click(object sender, EventArgs e)  //跳转至时刻表界面
        {
            Import_Schedule import_Schedule = new Import_Schedule();
            this.Hide();
            import_Schedule.ShowDialog();
            Application.ExitThread();
        }
    }
}