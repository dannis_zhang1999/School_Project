﻿using Calculate;
using System;
using System.Drawing;
using System.Windows.Forms;
using static TDSS.DataStore;
using static TDSS.DrawRedLine;
using static TDSS.STATIC;

namespace TDSS
{
    public static class DrawGreenLine
    {
        public static (Point[], Point, Point, Point, string) drawgreenline(string t/*延误车次号*/,string time/*延误时间*/)
        {
            int train = int.Parse(t);                                             //发生初始延误的列车号
            int StartIntervalBuffer = Delay.start_interval_buffer * 60;           //发车间隔缓冲时间，单位秒
            const string buffer = "00:01:00";                                     //发车间隔缓冲时间
            const string station_buffer = "00:00:10";                             //停站缓冲时间
            string dir = Direction_, type = DelayType, firstDelaySite = DelaySite;//延误方向、类型和站点
            int firstDelaySite_n = -1;                                            //车站序号
            int train_number = train + 1;                                         //发生后续延误的列车序号
            Point[] result;                                                       //结果数组
            string DELAYtime_0 = time;                                            //初始延误时间
            string DELAYtime_1;                                                   //后续列车延误时间
            int delaytime = int.Parse(DELAYtime_0.Substring(0, 2)) * 3600 +
                int.Parse(DELAYtime_0.Substring(3, 2)) * 60 + int.Parse(DELAYtime_0.Substring(6));
                                                                                  //delaytime 初始延误时间，单位秒
            Point point1 = new Point();                                           //连接辅助点1
            Point point2 = new Point();                                           //连接辅助点2
            Point leave_point = new Point();                                      //离开延误连接辅助点
            string adt = "";                                                      //实际出发时间

            for (int i = 0; i < 26; i++)//查找延误站点索引
            {
                if (Equals(firstDelaySite, Station[i]))
                {
                    firstDelaySite_n = i;
                    break;
                }
            }

            if (delaytime > 60)
            {
                if (firstDelaySite_n != -1)
                {
                    if (Equals(type, "到达延误"))
                    {
                        /*延误时间标准化*/
                        delaytime -= 60;
                        int min, s;
                        min = delaytime / 60;
                        s = delaytime % 60;
                        DELAYtime_1 = Time_Sub(DELAYtime_0, buffer);
                        int[] arrival_delaytime = Delay.Arrival_Delay(min, s).Item1;
                        int[] interval_delaytim = Delay.Arrival_Delay(min, s).Item2;
                        int[] vs = new int[arrival_delaytime.Length + interval_delaytim.Length];
                        add(ref vs, arrival_delaytime, interval_delaytim);

                        leave_point.X = leave_point.Y = 0;

                        if (Equals(dir, "往松江南站方向"))
                        {
                            if (delaytime < StartIntervalBuffer)
                            {
                                result = new Point[1];
                                point1.X = point2.X = result[0].X = 0;
                                point1.Y = point2.Y = result[0].Y = 0;
                            }
                            else
                            {
                                if (firstDelaySite_n > 0 && firstDelaySite_n < 25)
                                {
                                    int usefulLength = Math.Min(vs.Length, -2 * firstDelaySite_n + 50);
                                    result = new Point[usefulLength + 1];

                                    string the_first = Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n + 1].ToString();
                                    point2.X = result[0].X = Abscissa(Time_Add(the_first, DELAYtime_1));
                                    point2.Y = result[0].Y = stations[firstDelaySite_n];
                                    point1.X = Abscissa(the_first);
                                    point1.Y = stations[firstDelaySite_n];
                                    adt = Time_Add(Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n + 2].ToString(), DELAYtime_1);
                                    string the_data;
                                    for (int i = 1; i <= usefulLength; i++)
                                    {
                                        the_data = Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n + 1 + i].ToString();
                                        DELAYtime_1 = Time_Sub(DELAYtime_1, turn(0, 0, vs[i]));
                                        result[i].X = Abscissa(Time_Add(the_data, DELAYtime_1));
                                        result[i].Y = stations[firstDelaySite_n + i / 2];
                                    }
                                }
                                else
                                    throw new IndexOutOfRangeException("车站设置不合法");
                            }
                        }
                        else//往杨高中路方向
                        {
                            if (delaytime < StartIntervalBuffer)
                            {
                                result = new Point[1];
                                result[0].X = 0;
                                result[0].Y = 0;
                            }
                            else
                            {
                                if (firstDelaySite_n > 0 && firstDelaySite_n < 25)
                                {
                                    int usefulLength = Math.Min(vs.Length, 2 * firstDelaySite_n);
                                    result = new Point[usefulLength + 1];

                                    string the_first = Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 51].ToString();
                                    point2.X = result[0].X = Abscissa(Time_Add(the_first, DELAYtime_1));
                                    point2.Y = result[0].Y = stations[firstDelaySite_n];
                                    point1.X = Abscissa(the_first);
                                    point1.Y = stations[firstDelaySite_n];
                                    adt = Time_Add(Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 52].ToString(), DELAYtime_1);
                                    string the_data;
                                    for (int i = 1; i <= usefulLength; i++)
                                    {
                                        the_data = Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 51 + i].ToString();
                                        DELAYtime_1 = Time_Sub(DELAYtime_1, turn(0, 0, vs[i]));
                                        result[i].X = Abscissa(Time_Add(the_data, DELAYtime_1));
                                        result[i].Y = stations[firstDelaySite_n - i / 2];
                                    }
                                }
                                else
                                    throw new IndexOutOfRangeException("车站设置不合法");
                            }
                        }
                    }
                    else//离开延误
                    {
                        delaytime -= 60;
                        int min, s;
                        min = delaytime / 60;
                        s = delaytime % 60;
                        DELAYtime_1 = Time_Sub(DELAYtime_0, buffer);

                        int[] interval_delaytime = Delay.Leave_Delay(min, s).Item1;
                        int[] leave_delaytime = Delay.Leave_Delay(min, s).Item2;
                        int[] vs = new int[interval_delaytime.Length + leave_delaytime.Length];
                        add(ref vs, interval_delaytime, leave_delaytime);

                        if (Equals(dir, "往松江南站方向"))
                        {
                            if (delaytime < StartIntervalBuffer)
                            {
                                result = new Point[1];
                                point1.X = point2.X = result[0].X = 0;
                                point1.Y = point2.Y = result[0].Y = 0;
                            }
                            else
                            {
                                if (firstDelaySite_n > 0 && firstDelaySite_n < 25)
                                {
                                    int usefulLength = Math.Min(vs.Length, -2 * firstDelaySite_n + 49);
                                    result = new Point[usefulLength + 1];

                                    string the_first = Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n + 2].ToString();
                                    string the_first_1 = Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n + 1].ToString();
                                    point2.X = result[0].X = Abscissa(Time_Add(the_first, DELAYtime_1));
                                    point2.Y = result[0].Y = stations[firstDelaySite_n];
                                    leave_point.X = Abscissa(Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n].ToString());
                                    leave_point.Y = stations[firstDelaySite_n - 1];
                                    adt = Time_Add(the_first, DELAYtime_1);
                                    if (Time_Sub(actual_departure_time, the_first_1) != "")
                                    {
                                        point1.X = Abscissa(Time_Add(the_first_1, Time_Sub(DELAYtime_1, station_buffer)));
                                        point1.Y = stations[firstDelaySite_n];
                                    }
                                    else
                                    {
                                        point1.X = Abscissa(the_first_1);
                                        point1.Y = stations[firstDelaySite_n];
                                    }

                                    string the_data;
                                    for (int i = 1; i <= usefulLength; i++)
                                    {
                                        the_data = Time_table.Tables[0].Rows[train_number - 1][2 * firstDelaySite_n + 2 + i].ToString();
                                        DELAYtime_1 = Time_Sub(DELAYtime_1, turn(0, 0, vs[i - 1]));
                                        result[i].X = Abscissa(Time_Add(the_data, DELAYtime_1));
                                        result[i].Y = stations[firstDelaySite_n + (i + 1) / 2];
                                    }
                                }
                                else
                                    throw new IndexOutOfRangeException("车站设置不合法");
                            }
                        }
                        else//往杨高中路方向
                        {
                            if (delaytime < StartIntervalBuffer)
                            {
                                result = new Point[1];
                                point1.X = point2.X = result[0].X = 0;
                                point1.Y = point2.Y = result[0].Y = 0;
                            }
                            else
                            {
                                if (firstDelaySite_n > 0 && firstDelaySite_n < 25)
                                {
                                    int usefulLength = Math.Min(vs.Length, 2 * firstDelaySite_n - 1);
                                    result = new Point[usefulLength + 1];

                                    string the_first = Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 52].ToString();
                                    string the_first_1 = Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 51].ToString();
                                    point2.X = result[0].X = Abscissa(Time_Add(the_first, DELAYtime_1));
                                    point2.Y = result[0].Y = stations[firstDelaySite_n];
                                    leave_point.X = Abscissa(Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 50].ToString());
                                    leave_point.Y = stations[firstDelaySite_n + 1];
                                    adt = Time_Add(the_first, DELAYtime_1);
                                    if (Time_Sub(actual_departure_time, the_first_1) != "")
                                    {
                                        point1.X = Abscissa(Time_Add(the_first_1, Time_Sub(DELAYtime_1, station_buffer)));
                                        point1.Y = stations[firstDelaySite_n];
                                    }
                                    else
                                    {
                                        point1.X = Abscissa(the_first_1);
                                        point1.Y = stations[firstDelaySite_n];
                                    }

                                    string the_data;
                                    for (int i = 1; i <= usefulLength; i++)
                                    {
                                        the_data = Time_table.Tables[1].Rows[train_number - 1][-2 * firstDelaySite_n + 52 + i].ToString();
                                        DELAYtime_1 = Time_Sub(DELAYtime_1, turn(0, 0, vs[i - 1]));
                                        result[i].X = Abscissa(Time_Add(the_data, DELAYtime_1));
                                        result[i].Y = stations[firstDelaySite_n - (i + 1) / 2];
                                    }
                                }
                                else
                                    throw new IndexOutOfRangeException("车站设置不合法");
                            }
                        }
                    }
                }
                else
                {
                    result = new Point[1];
                    result[0].Y = result[0].X = 0;
                }
            }
            else
            {
                result = new Point[1];
                result[0].Y = result[0].X = 0;
            }
            return (result, point2, point1, leave_point, adt);
        }
    }
}
