﻿namespace TDSS
{
    partial class Setup_Parameters
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Setup_Parameters));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SaveData = new System.Windows.Forms.Button();
            this.Schedule2 = new System.Windows.Forms.Button();
            this.site = new System.Windows.Forms.ComboBox();
            this.type = new System.Windows.Forms.ComboBox();
            this.train = new System.Windows.Forms.TextBox();
            this.min = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.s = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.directioncombox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gainsboro;
            this.label1.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(483, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "延误类型";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label2.Location = new System.Drawing.Point(160, 204);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 14);
            this.label2.TabIndex = 1;
            this.label2.Text = "延误站点";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(160, 350);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 2;
            this.label3.Text = "延误车次";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Gainsboro;
            this.label4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(483, 285);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 14);
            this.label4.TabIndex = 3;
            this.label4.Text = "延误时间";
            // 
            // SaveData
            // 
            this.SaveData.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SaveData.Location = new System.Drawing.Point(482, 426);
            this.SaveData.Name = "SaveData";
            this.SaveData.Size = new System.Drawing.Size(132, 46);
            this.SaveData.TabIndex = 12;
            this.SaveData.Text = "保存数据并作图";
            this.SaveData.UseVisualStyleBackColor = true;
            this.SaveData.Click += new System.EventHandler(this.SaveData_Click);
            // 
            // Schedule2
            // 
            this.Schedule2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Schedule2.Location = new System.Drawing.Point(291, 426);
            this.Schedule2.Name = "Schedule2";
            this.Schedule2.Size = new System.Drawing.Size(136, 46);
            this.Schedule2.TabIndex = 14;
            this.Schedule2.Text = "返回";
            this.Schedule2.UseVisualStyleBackColor = true;
            this.Schedule2.Click += new System.EventHandler(this.Schedule2_Click);
            // 
            // site
            // 
            this.site.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.site.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.site.FormattingEnabled = true;
            this.site.Items.AddRange(new object[] {
            "杨高中路",
            "世纪大道",
            "商城路",
            "小南门",
            "陆家浜",
            "马当路",
            "打浦桥",
            "嘉善路",
            "肇家浜",
            "徐家汇",
            "宜山路",
            "桂林路",
            "漕河泾开发区",
            "合川路",
            "星中路",
            "七宝",
            "中春路",
            "九亭",
            "泗泾",
            "佘山",
            "洞泾",
            "松江大学城",
            "松江新城",
            "松江体育中心",
            "醉白池",
            "松江南站"});
            this.site.Location = new System.Drawing.Point(253, 201);
            this.site.Name = "site";
            this.site.Size = new System.Drawing.Size(124, 20);
            this.site.TabIndex = 15;
            // 
            // type
            // 
            this.type.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.type.FormattingEnabled = true;
            this.type.Items.AddRange(new object[] {
            "离开延误",
            "到达延误"});
            this.type.Location = new System.Drawing.Point(574, 201);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(124, 20);
            this.type.TabIndex = 16;
            // 
            // train
            // 
            this.train.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.train.Location = new System.Drawing.Point(253, 347);
            this.train.Name = "train";
            this.train.Size = new System.Drawing.Size(124, 21);
            this.train.TabIndex = 17;
            // 
            // min
            // 
            this.min.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.min.Location = new System.Drawing.Point(574, 282);
            this.min.Name = "min";
            this.min.Size = new System.Drawing.Size(40, 21);
            this.min.TabIndex = 18;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(620, 285);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 19;
            this.label5.Text = "分钟";
            // 
            // s
            // 
            this.s.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.s.Location = new System.Drawing.Point(654, 282);
            this.s.Name = "s";
            this.s.Size = new System.Drawing.Size(44, 21);
            this.s.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(704, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "秒";
            // 
            // directioncombox
            // 
            this.directioncombox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.directioncombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.directioncombox.FormattingEnabled = true;
            this.directioncombox.Items.AddRange(new object[] {
            "往杨高中路方向",
            "往松江南站方向"});
            this.directioncombox.Location = new System.Drawing.Point(253, 282);
            this.directioncombox.Name = "directioncombox";
            this.directioncombox.Size = new System.Drawing.Size(124, 20);
            this.directioncombox.TabIndex = 22;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(160, 285);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 14);
            this.label7.TabIndex = 23;
            this.label7.Text = "方向";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(383, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(341, 12);
            this.label8.TabIndex = 25;
            this.label8.Text = "往松江南站方向，车次号不超过175；往杨高中路方向不超过169";
            // 
            // Setup_Parameters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(847, 521);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.directioncombox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.s);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.min);
            this.Controls.Add(this.train);
            this.Controls.Add(this.type);
            this.Controls.Add(this.site);
            this.Controls.Add(this.Schedule2);
            this.Controls.Add(this.SaveData);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Setup_Parameters";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "列车运行延误仿真系统";
            this.Load += new System.EventHandler(this.Setup_Parameters_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button SaveData;
        private System.Windows.Forms.Button Schedule2;
        private System.Windows.Forms.ComboBox site;
        private System.Windows.Forms.ComboBox type;
        private System.Windows.Forms.TextBox train;
        private System.Windows.Forms.TextBox min;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox s;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox directioncombox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}