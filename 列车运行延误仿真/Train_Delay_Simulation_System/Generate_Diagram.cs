﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using static TDSS.DataStore;
using static TDSS.DrawRedLine;
using static TDSS.STATIC;

namespace TDSS
{
    public partial class Generate_Diagram : Form
    {
        public Generate_Diagram()
        {
            InitializeComponent();
        }
       
        Bitmap bitmap;
        int u;


        private void Generate_Diagram_Load(object sender, EventArgs e)
        {
            this.MaximizeBox = false;

            try
            {
                Bitmap bitmap = (Bitmap)Image.FromFile(Application.StartupPath + "\\Di_Tu.jpg");
                pictureBox1.Image = bitmap;
                pictureBox1.Height = bitmap.Height;
                pictureBox1.Width = bitmap.Width;
            }
            catch (FileNotFoundException ex1)
            {
                MessageBox.Show(ex1.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                try
                {
                    Process pro = new Process();
                    pro.StartInfo.FileName = Application.StartupPath + "\\DrawPicture.exe";
                    pro.Start();
                    pro.Close();
                    MessageBox.Show("已重新生成底图", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Win32Exception ex2)
                {
                    MessageBox.Show(ex2.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(-1);
                }
            }
            catch (OutOfMemoryException ex3)
            {
                MessageBox.Show(ex3.Message, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Environment.Exit(-1);
            }
            catch (ArgumentException ex4)
            {
                MessageBox.Show(ex4.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }
            finally
            {
                try
                {
                    Bitmap bitmap = (Bitmap)Image.FromFile(Application.StartupPath + "\\Di_Tu.jpg");
                    pictureBox1.Image = bitmap;
                    pictureBox1.Height = bitmap.Height;
                    pictureBox1.Width = bitmap.Width;
                }
                catch (FileNotFoundException)
                {
                    Environment.Exit(-1);
                }
            }

            int[] station = {38, 58, 70, 90, 103, 113, 125, 136, 147, 160, 174, 188, 205,
                             217, 233, 250, 262, 283, 324, 352, 369, 393, 415, 431, 448, 463};

            Pen RedPen = new Pen(Color.Red, 2);
            Pen BlackPen = new Pen(Color.Black, 2);
            Pen GreenPen = new Pen(Color.Green, 2);
            Bitmap bt = new Bitmap(pictureBox1.Image);
            Graphics g = Graphics.FromImage(bt);
            g.SmoothingMode = SmoothingMode.HighQuality;
            Point[] points = new Point[station.Length * 2 - 1];
            int number = 0;   //后续延误列车数

            //延误修正作图
            try
            {
                Point[] redLine = draw(DelayType, DelayTime_min, DelayTime_s, DelaySite, Direction_, int.Parse(DelayTrain)).Item1;
                Point a = draw(DelayType, DelayTime_min, DelayTime_s, DelaySite, Direction_, int.Parse(DelayTrain)).Item2;
                Point b = draw(DelayType, DelayTime_min, DelayTime_s, DelaySite, Direction_, int.Parse(DelayTrain)).Item3;
                u = draw(DelayType, DelayTime_min, DelayTime_s, DelaySite, Direction_, int.Parse(DelayTrain)).Item4;
                g.DrawLines(RedPen, redLine);
                g.DrawLine(RedPen, a, b);

                Point[] greenLine;
                Point h, i, j;
                string adt;
                if (Time_Sub(DELAYTIME, "00:01:00") == "")
                    MessageBox.Show("对后续列车不产生影响");

                do
                {
                    greenLine = DrawGreenLine.drawgreenline(Train, DELAYTIME).Item1;//result[]
                    h = DrawGreenLine.drawgreenline(Train, DELAYTIME).Item2;//point2
                    i = DrawGreenLine.drawgreenline(Train, DELAYTIME).Item3;//point1
                    j = DrawGreenLine.drawgreenline(Train, DELAYTIME).Item4;//leave_point
                    adt = DrawGreenLine.drawgreenline(Train, DELAYTIME).Item5;//adt
                    g.DrawLines(GreenPen, greenLine);
                    g.DrawLine(GreenPen, i, h);
                    if (DelayType == "离开延误")
                        g.DrawLine(GreenPen, i, j);

                    Train = (int.Parse(Train) + 1).ToString();
                    DELAYTIME = Time_Sub(DELAYTIME, "00:01:00");
                    actual_departure_time = adt;
                    number++;

                } while (Time_Sub(DELAYTIME, "00:01:00") != "");

        }
            catch (Exception) when (AbnormalMessagePrompt == true)
            { }
            catch (OverflowException ex)
            { MessageBox.Show(ex.Message); }
            catch (FormatException)
            { MessageBox.Show("延误数据不完整", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            catch (IndexOutOfRangeException)
            { MessageBox.Show("已取消延误设置", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information); }
            catch (ArgumentException ex)
            { MessageBox.Show(ex.Message); }
            finally   //运行时刻表作图
            {
                try
                {
                    //杨高中路->松江南站
                    for (int j = 0; j < 175; j++)
                    {
                        for (int i = 1; i < 52; i++)
                        {
                            string str = Time_table.Tables[0].Rows[j][i].ToString();
                            points[i - 1].X = Abscissa(str);
                            points[i - 1].Y = station[(i - 1) / 2];
                        }
                        g.DrawLines(BlackPen, points);
                    }
                    //松江南站->杨高中路
                    for (int j = 0; j < 169; j++)
                    {
                        for (int i = 1; i < 52; i++)
                        {
                            string str = Time_table.Tables[1].Rows[j][i].ToString();
                            points[i - 1].X = Abscissa(str);
                            points[i - 1].Y = station[25 - ((i - 1) / 2)];
                        }
                        g.DrawLines(BlackPen, points);
                    }
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }

            pictureBox1.Image = bt;
            bitmap = bt;
            
            g.Dispose();
            RedPen.Dispose();
            BlackPen.Dispose();

            //信息显示
            try
            {
                string[] s1 = new string[7] {"延误车次号：","延误时长：","延误站点：",
                                             "延误类型：","列车方向：","受影响车站数：","受影响列车数：" };
                string[] s2 = new string[7];
                s2[0] = DelayTrain;
                s2[1] = turn(new_turn(DelayTime_min, DelayTime_s).Item1, new_turn(DelayTime_min, DelayTime_s).Item2, new_turn(DelayTime_min, DelayTime_s).Item3);
                s2[2] = DelaySite;
                s2[3] = DelayType;
                s2[4] = Direction_;
                int st;
                if (Equals(DelayType, "离开延误"))
                {
                    if (u % 2 == 0)
                        st = u / 2;
                    else
                        st = u / 2 + 1;
                }
                else
                {
                    if (u % 2 == 0)
                        st = u / 2;
                    else
                        st = (u - 1) / 2;
                }
                s2[5] = (st + 1).ToString();
                s2[6] = number.ToString();

                string[] s = new string[7];
                for (int i = 0; i < 7; i++)
                    s[i] = s1[i] + s2[i];
                label1.Text = s[0]; label4.Text = s[3];
                label2.Text = s[1]; label5.Text = s[4];
                label3.Text = s[2]; label6.Text = s[5];
                label7.Text = s[6];
            }
            catch (FormatException)
            {
                MessageBox.Show("延误数据不完整", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void Parameters3_Click(object sender, EventArgs e)
        {
            Setup_Parameters setup_Parameters = new Setup_Parameters();
            this.Hide();
            setup_Parameters.ShowDialog();
        }

        private void Schedule3_Click(object sender, EventArgs e)
        {
            Import_Schedule import_Schedule = new Import_Schedule();
            this.Hide();
            import_Schedule.ShowDialog();
        }

        private void SavePicture_Click(object sender, EventArgs e)
        {
            if (comboBox.SelectedItem != null)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Title = "另存为";
                save.OverwritePrompt = true;
                save.CheckPathExists = true;
                save.Filter = comboBox.Text + "|" + comboBox.Text;
                if (save.ShowDialog() == DialogResult.OK)
                {
                    bitmap.Save(save.FileName, ImageFormat.Jpeg);
                    MessageBox.Show("运行图已保存", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("已取消保存", "", MessageBoxButtons.OK, MessageBoxIcon.Information);              
                save.Dispose();
            }
            else
                MessageBox.Show("请选择保存类型", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }       
    }
}