﻿using System;
using System.Data;

namespace TDSS
{
    public static class DataStore
    {
        public static DataSet Time_table = null;
        
        public static string DelayTrain = "";
        public static int DelayTime_min = 0;
        public static int DelayTime_s = 0;
        public static string DelaySite = "";
        public static string DelayType = "";
        public static string Direction_ = "";

        public static bool AbnormalMessagePrompt = true;

//-------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------//

        public static int Abscissa(string data) //由时间计算横坐标
        {
            int hour = int.Parse(data.Substring(0, 2));
            int min = int.Parse(data.Substring(3,2));
            int s = int.Parse(data.Substring(6));
            if (hour < 5) hour += 24;
            int x = 150 + 12 * 60 * (hour - 5) + 12 * (min - 30) + (int)((((double)s) / 60.0) * 12.0);  //首班车时间为每天早5:30
            return x;
        }
//--------------------------------------------------------------------------------------------------------//
        //将数据存储到静态类中，2个重载
        public static void Get_data(DataSet ds) => Time_table = ds;
        public static void Get_data(string DTrain, string DTime_min, string DTime_s, string DSite, string DType, string Direction)
        {
            try
            {
                DelayTrain = DTrain;
                DelayTime_min = int.Parse(DTime_min);
                DelayTime_s = int.Parse(DTime_s);
                DelaySite = DSite;
                DelayType = DType;
                Direction_ = Direction;
            }
            catch (FormatException)
            {
                System.Windows.Forms.MessageBox.Show("数据格式不正确");
            }
        }
//--------------------------------------------------------------------------------------------------------//
        private static void fortmat(ref string s) //时间数据格式规范化
        {
            try
            {
                if (int.Parse(s) < 10) 
                    s = "0" + s;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "错误");
            }
        }          

        public static string Time_Add(params string[] str) //时间相加
        {
            int sum = 0;
            string result, h, min, s;
            foreach (string v in str)
                sum += int.Parse(v.Substring(0, 2)) * 3600 + int.Parse(v.Substring(3, 2)) * 60 + int.Parse(v.Substring(6));
            h = (sum / 3600).ToString();
            min = ((sum % 3600) / 60).ToString();
            s = (sum % 60).ToString();
            fortmat(ref h); fortmat(ref min); fortmat(ref s);
            return result = h + ":" + min + ":" + s;
        }

        public static string Time_Sub(string str1, string str2)  //时间相减
        {            
            string result, h, min, s;
            int sum = int.Parse(str1.Substring(0, 2)) * 3600 + int.Parse(str1.Substring(3, 2)) * 60 + int.Parse(str1.Substring(6));
            int sub = int.Parse(str2.Substring(0, 2)) * 3600 + int.Parse(str2.Substring(3, 2)) * 60 + int.Parse(str2.Substring(6));
            sum = sum - sub;
            if (sum >= 0)
            {
                h = (sum / 3600).ToString();
                min = ((sum % 3600) / 60).ToString();
                s = (sum % 60).ToString();
                fortmat(ref h); fortmat(ref min); fortmat(ref s);
                result = h + ":" + min + ":" + s;
            }
            else
                result = "";
            return result;
        }
    }
}