﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace DrawPicture
{
    public partial class DrawPicture : Form
    {
        public DrawPicture()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Pen myPen = new Pen(Color.Blue, 1);
                Bitmap bitmap = (Bitmap)Image.FromFile(Application.StartupPath + "\\background.jpg");
                Graphics g = Graphics.FromImage(bitmap);
                int w = bitmap.Width;

                //绘制边框线
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawLine(myPen, new Point(150, 38), new Point(w, 38));
                g.DrawLine(myPen, new Point(150, 38), new Point(150, 463));
                g.DrawLine(myPen, new Point(150, 463), new Point(w, 463));
                
                //绘制时刻线
                for (int x = 150; x <= w; x += 12)              
                    g.DrawLine(myPen, new Point(x, 38), new Point(x, 463));
                

                int[] station = {38, 58, 70, 90, 103, 113, 125, 136, 147, 160, 174, 188,
                    205, 217, 233, 250,262, 283, 324, 352, 369, 393, 415, 431, 448,463 };

                //绘制车站中心线
                foreach (int s in station)
                    g.DrawLine(myPen, new Point(150, s), new Point(w, s));              

                //绘制标识文字
                string[] station_name = new string[] { "杨高中路", "世纪大道", "商城路", "小南门", "陆家浜", "马当路", "打浦桥", "嘉善路",
                                                       "肇家浜", "徐家汇", "宜山路", "桂林路", "漕河泾开发区", "合川路", "星中路", "七宝",
                                                       "中春路", "九亭", "泗泾", "佘山", "洞泾", "松江大学城", "松江新城", "松江体育中心",
                                                       "醉白池", "松江南站"};
                Font font1 = new Font("宋体", 8);
                Font font2 = new Font("Times New Roman", 8);
                SolidBrush brush = new SolidBrush(Color.Black);
                string str, _str; int k = 0;
                for (int i = 0; i < 26; i++)                
                    g.DrawString(station_name[i], font1, brush, 95, station[i]);
                for (int hour = 5; hour < 24; hour++)
                {
                    switch (hour)
                    {
                        case 5:
                            for (int min = 30; min <= 55; min += 5)
                            {                                
                                str = hour.ToString() + ":" + min.ToString();
                                g.DrawString(str, font2, brush, 148 + k * 12 * 5, 465);
                                k++;
                            }
                            break;
                        default:
                            for (int min = 0; min <= 55; min += 5)
                            {
                                if (min == 0 || min == 5)
                                    _str = "0" + min.ToString();
                                else
                                    _str = min.ToString();
                                str = hour.ToString() + ":" + _str;
                                g.DrawString(str, font2, brush, 148 + k * 12 * 5, 465);
                                k++;
                            }
                            break;
                    }
                }
                
                //保存底图
                bitmap.Save(Application.StartupPath + "\\Di_Tu.jpg", ImageFormat.Jpeg);
                bitmap.Dispose();

                Environment.Exit(0);
            }
            catch(FileNotFoundException)
            {
                MessageBox.Show("资源文件丢失", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }
        }
    }
}