using System;
using System.Collections.Generic;

namespace CODE
{
    class Program
    {
        static void Main(string[] args)
        {
            const int v = 40;                         //列车运行速度，km/h
            const int door = 5;                       //车门开启、关闭时间
            int[] K = new int[3] { 1, 2, 3 };         //车站集合
            int k, f, r;                              //车站索引，方向索引，列车索引
            int[] F = new int[2] { 1, 2 };            //运行方向集合
            int[] m1 = new int[2] { 2400, 3000 };     //车站距离（上行）,m
            int[] m2 = new int[2] { 3000, 2400 };     //车站距离（下行）,m
            const int C = 1000;                       //列车满载乘客数
            const int h_min = 120, h_max = 600;       //最小、最大发车间隔
            const int T = 5400;                       //研究时段
            const int t_min = 180;                    //最小折返时间
            const int omega_min = 20, omega_max = 180;//乘客能接受的最小、最大停站时间
            MAX_OR_MIN MM = new MAX_OR_MIN();

            //OD矩阵
            int[] OD1 = new int[3] { 0,    150, 120 };
            int[] OD2 = new int[3] { 130,  0,   50 };
            int[] OD3 = new int[3] { 250,  60,  0 };
            List<int[]> OD = new List<int[]>();
            OD.Add(OD1);
            OD.Add(OD2);
            OD.Add(OD3);
            int[] upWardFlow = new int[] { OD1[1], OD1[2], OD2[2] };  //上行客流
            int[] downWardFlow = new int[] { OD2[0], OD3[0], OD3[1] };//下行客流
            int D1 = MM.MAX(MM.MAX(upWardFlow) / C, T / h_max);       //上行计划开行列车数
            int D2 = MM.MIN(MM.MIN(downWardFlow) / C, T / h_max);     //下行计划开行列车数

            int omega = (omega_max + omega_min) / 2;                  //列车平均停站时间
            int[] R1 = new int[2];                                    //列车在区间行驶时间，上行
            int[] R2 = new int[2];                                    //列车在区间行驶时间，下行
            for (int i = 0; i <= m1.Length - 1; i++)
            {
                R1[i] = m1[i] / (v * 1000 / 3600);
                R2[i] = m2[i] / (v * 1000 / 3600);
            }

            int n1 = D1 * (MM.ADD(R1) + t_min + K.Length * omega) / T;//上行需要列车数
            int n2 = D2 * (MM.ADD(R2) + t_min + K.Length * omega) / T;//下行需要列车数


            int[] a1 = new int[K.Length]; int[] d1 = new int[K.Length];//上行列车到站与离站时刻
            int[] a2 = new int[K.Length]; int[] d2 = new int[K.Length];//下行列车到站与离站时刻

            int[] b1 = new int[K.Length]; int[] b2 = new int[K.Length];//上、下行列车到站时车上的乘客
            int[] c1 = new int[K.Length]; int[] c2 = new int[K.Length];//上、下行方向各车站候车的乘客
            int[] e1 = new int[K.Length]; int[] e2 = new int[K.Length];//上、下行列车到站时下车的乘客
            a1[0] = a2[0] = 0; d1[0] = d2[0] = 0 + omega;
            b1[0] = b2[0] = 0;
            e1[0] = 0; e1[1] = OD1[1]; e1[2] = OD1[2] + OD2[2];
            e2[0] = 0; e2[1] = OD3[1]; e2[2] = OD2[0] + OD3[0];
            c1[0] = OD1[1] + OD1[2]; c1[1] = OD2[2]; c1[2] = 0;
            c2[0] = OD3[0] + OD3[1]; c2[1] = OD2[0];c2[2] = 0;

            int s1 = C - b1[0];//车上剩余空位
            int s2 = C - b2[0];
            for (int i = 1; i < K.Length; i++)
            {
                if (b1[i - 1] <= C)
                {
                    s1 = C - b1[i - 1] + e1[i];
                    if (s1 > c1[i])
                    {
                        b1[i] = c1[i] + b1[i - 1] - e1[i];
                        s1 = C - b1[i];
                    }
                    else
                    {
                        c1[i] -= s1;
                        b1[i] = b1[i - 1] + s1 - e1[i];
                    }
                }
                else
                    b1[i] = b1[i - 1] - e1[i];
            }
            for (int i = 1; i < K.Length; i++)
            {
                if (b2[i - 1] <= C)
                {
                    s2 = C - b2[i - 1];
                    if (s2 > c2[i])
                    {
                        b2[i] = c2[i] + b2[i - 1] - e2[i];
                        s2 = C - b2[i];
                    }
                    else
                    {
                        c2[i] -= s2;
                        b2[i] = b2[i - 1] + s2 - e2[i];
                    }
                }
                else
                    b2[i] = b2[i - 1] - e2[i];
            }

            List<int> TimeTable1 = new List<int>();
            List<int> TimeTable2 = new List<int>();
            List<int> newTimeTable1 = new List<int>();
            List<int> newTimeTable2 = new List<int>();
            Adjust_Omega adjust = new Adjust_Omega();
            for (k = 1; k < K.Length; k++)
            {
                a1[k] = d1[k - 1] + R1[k - 1];
                a2[k] = d2[k - 1] + R2[k - 1];
                d1[k] = a1[k] + omega;
                d2[k] = a2[k] + omega;
            }
            for (int i = 0; i < K.Length; i++)
            {
                TimeTable1.Add(a1[i]);
                TimeTable1.Add(d1[i]);
                TimeTable2.Add(a2[i]);
                TimeTable2.Add(d2[i]);
            }

            d1[0] = 0 + adjust.OMEGA(e1[0], door, omega_min, omega_max);
            d2[0] = 0 + adjust.OMEGA(e2[0], door, omega_min, omega_max);
            for (k = 1; k < K.Length; k++)
            {
                a1[k] = d1[k - 1] + R1[k - 1];
                a2[k] = d2[k - 1] + R2[k - 1];
                d1[k] = a1[k] + adjust.OMEGA(e1[k], door, omega_min, omega_max);
                d2[k] = a2[k] + adjust.OMEGA(e2[k], door, omega_min, omega_max);
            }
            for (int i = 0; i < K.Length; i++)
            {
                newTimeTable1.Add(a1[i]);
                newTimeTable1.Add(d1[i]);
                newTimeTable2.Add(a2[i]);
                newTimeTable2.Add(d2[i]);
            }

            Console.WriteLine("原时刻表：\n");
            Console.WriteLine("上行\n");
            for (int i = 0; i < TimeTable1.Count; i += 2)
                Console.WriteLine("{0}   {1}\n", TimeTable1[i], TimeTable1[i + 1]);
            Console.WriteLine("下行\n");
            for (int i = 0; i < TimeTable2.Count; i += 2)
                Console.WriteLine("{0}   {1}\n", TimeTable2[i], TimeTable2[i + 1]);
            
            Console.WriteLine("新时刻表：\n");
                Console.WriteLine("上行\n");
            for (int i = 0; i < newTimeTable1.Count; i += 2)
                Console.WriteLine("{0}   {1}\n", newTimeTable1[i], newTimeTable1[i + 1]);
            Console.WriteLine("下行\n");
            for (int i = 0; i < newTimeTable2.Count; i += 2)
                Console.WriteLine("{0}   {1}\n", newTimeTable2[i], newTimeTable2[i + 1]); 
        }      
    }
}
