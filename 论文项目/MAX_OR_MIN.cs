﻿using System;

namespace CODE
{
    class MAX_OR_MIN
    {
        public int MAX(params int[] prop)
        {
            int max = prop[0];
            foreach (int i in prop)
            {
                if (i > max)
                    max = i;
            }
            return max;
        }

        public int MIN(params int[] prop)
        {
            int min = prop[0];
            foreach (int i in prop)
            {
                if (i < min)
                    min = i;
            }
            return min;
        }

        public int ADD(params int[] vs)
        {
            int r = 0;
            foreach (int i in vs)
                r += i;
            return r;
        }
    }
}
