﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CODE
{
    class Adjust_Omega
    {
        public int OMEGA(int q, int door, int omega_min, int omega_max)
        {
            int t;
            t = (int)((0.4331 * q + 0.632) + 2 * door + 20);
            if (t > omega_max)
                t = omega_max;
            else if (t < omega_min)
                t = omega_min;
            return t;
        }
    }
}
