﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Windows.Forms;

namespace ConnentAccess
{
    /// <summary>
    /// 与用户数据相关的数据库操作
    /// </summary>
    public class Connect_User : ConnectBase
    {
        internal static string Level; ///权限等级
        public static string name, pwd;

        /// <summary>
        /// 验证用户名与密码，并返回用户权限，默认权限为0
        /// </summary>
        public (bool, string) VerifyUserName(string name,string password)
        {
            try
            { OleCon.Open(); }
            catch (InvalidOperationException ex1)
            { MessageBox.Show(ex1.Message); }
            catch (OleDbException ex2)
            { MessageBox.Show(ex2.Message); }
                       
            bool s = false; string level = "0";
            string sql = "select Password from UserInfor where Wno='" + name + "'";
            OleDbDataAdapter oleDap = new OleDbDataAdapter(sql, OleCon);
            DataSet ds1 = new DataSet();
            oleDap.Fill(ds1, "UserInfor");
            if (ds1.Tables[0].Rows.Count > 0)
            {
                string pwd = ds1.Tables[0].Rows[0]["Password"].ToString();
                if (String.Equals(password, pwd) == true)
                {
                    MessageBox.Show("验证成功", "欢迎", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    s = true;
                    sql = "select Authority from UserInfor where Wno='" + name + "'";
                    oleDap = new OleDbDataAdapter(sql, OleCon);
                    DataSet ds2 = new DataSet();
                    oleDap.Fill(ds2, "UserInfor");
                    level = ds2.Tables[0].Rows[0]["Authority"].ToString();
                    ds2.Dispose();
                }
                else
                    MessageBox.Show("密码错误", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                MessageBox.Show("用户名不存在", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            ds1.Dispose();
            oleDap.Dispose();
            Level = level;

            return (s, level);
        }

        /// <summary>
        /// 析构器
        /// </summary>
        ~Connect_User()
        {
            name = null;
            pwd = null;
        }
    }
}
