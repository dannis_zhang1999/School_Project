﻿using System.Data.OleDb;
using System.Windows.Forms;

namespace ConnentAccess
{
    /// <summary>
    /// 连接基类
    /// </summary>
    public class ConnectBase
    {
        /// <summary>
        /// 数据库连接
        /// </summary>       
        internal static string connection_string = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Application.StartupPath + "\\station.mdb";
        internal OleDbConnection OleCon = new OleDbConnection(connection_string);
    }
}
