﻿using ConnentAccess;
using System;
using System.Data.OleDb;
using System.Windows.Forms;

namespace UI
{
    public partial class Log_in : Form
    {
        public Log_in()
        {
            InitializeComponent();
        }

        private void Verify_Click(object sender, EventArgs e)
        {
            try
            {
                string userName, passWord;
                bool s;
                userName = user_name.Text.Trim();
                passWord = password.Text.Trim();
                Connect_User connect_Uesr = new Connect_User();
                s = connect_Uesr.VerifyUserName(userName, passWord).Item1;
                Level.level = connect_Uesr.VerifyUserName(userName, passWord).Item2;

                if (s == true)
                {
                    Stations stations = new Stations();
                    this.Hide();
                    stations.ShowDialog();
                    Application.ExitThread();
                }
                else
                {
                    MessageBox.Show("登录失败", "❌", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    user_name.Text = "";
                    password.Text = "";
                }
            }
            catch (OleDbException err)
            { MessageBox.Show(err.Message); }
        }

        private void Log_in_Load(object sender, EventArgs e)
        {
            MessageBox.Show("欢迎使用地铁车站风险点分析与预警系统！", "欢迎", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.MaximizeBox = false;
        }

        private void Passengers_query_Click(object sender, EventArgs e)
        {
            Stations stations = new Stations();
            this.Hide();
            stations.ShowDialog();
            Application.ExitThread();
        }
    }
}
